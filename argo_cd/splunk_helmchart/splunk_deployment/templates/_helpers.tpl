# A place to put template helpers that you can re-use throughout the chart

{{- define "indexers" -}}     {{- /* define template name */ -}}
{{- $ind_num := .indexer.replicas | int }}     {{- /* ind_num is a number of indexer replicas */ -}}
    {{- range $ind := until $ind_num -}}      {{- /* loop from 0 to number of indexer replicas */ -}}
        {{- $ind1 := $ind | add1 -}}     {{- /* add1 function https://masterminds.github.io/sprig/math.html */ -}}
indexer-{{ $ind }}{{ if ne $ind1 $ind_num }},{{ end }}      {{- /* Creating comma separated list of indexers.Last value should not be separated by comma */ -}}
    {{- end -}}
{{ end }}