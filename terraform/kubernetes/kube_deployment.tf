resource "kubernetes_persistent_volume_claim" "postgres-storage" {
  metadata {
    name = "postgres-storage"
  }
  spec {
    access_modes = ["ReadWriteOnce"]
    resources {
      requests = {
        storage = "8Gi"
      }
    }
  }
}

resource "kubernetes_secret" "postgres-data" {
  metadata {
    name = "postgres-data"
  }
  type = "Opaque"
  data = {
    password = "c29uYXI="
    user = "c29uYXI="
  }
}


resource "kubernetes_deployment" "postgres" {
  metadata {
      name = "postgres"
      labels = {
          app = "postgres"
      }
  }
  spec {
    replicas = 1
    selector {
      matchLabels {
        app = "postgres"
      }
    }
    template {
      metadata {
        labels = {
          app = "postgres"
        }
      }
      spec {
        container {
          name = "postgres"
          image = "postgres:12"
          resource {
            requests {
              memory = "1Gi"
              cpu = "500m"
            }
            limits {
              memory = "2Gi"
              cpu = "1000m"
            }
          }
          port = 5432
          env = [
            {
              name = "POSTGRES_PASSWORD"
              value_from {
                secret_key_ref {
                  key = "user"
                  name = "postgres-data"
                }
              }
            },
            {
              name = "POSTGRES_DB"
              value = "sonar"
            },
            {
              name = "PGDATA"
              value = "/var/lib/postgresql/data/pgdata"
            }
          ]
          volume_mount {
            mount_path = "/var/lib/postgresql/data"
            name = "postgresdata"
          }
        }
        volume {
          name = "postgresdata"
          persistent_volume_claim {
            claim_name = "${kubernetes_persistent_volume_claim.postgres-storage.metadata.0.name}"
          }

        }
      }
    }
  }
}
