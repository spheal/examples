provider "google" {
    credentials = "${file("../keys/key.json")}"
    project = "${var.PROJECT}"
    region = "${var.REGION}"
}
resource "google_compute_instance" "gitrunner" {
    name = "gitrunner"
    machine_type = "g1-small"
    zone = "${var.REGION}-c"
    tags = ["ci","gitlab-runner"]
    boot_disk {
        auto_delete = true
        initialize_params {
            size = 10
            type = "pd-standard"
            image = "ubuntu-minimal-1904"
        }
    }
    network_interface {
        network = "default"
        access_config {}
    }
    scheduling {
        preemptible = true
        automatic_restart = false
    }
    description = "VM for running GitLabCI runner"
    metadata_startup_script = <<SHELL
    #!/bin/bash
    sudo apt update -y
    sudo apt -y install docker.io
    systemctl enable docker
    systemctl start docker
    sudo curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64 
    sudo chmod +x /usr/local/bin/gitlab-runner 
    sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash 
    sudo usermod -aG docker gitlab-runner
    sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner 
    sudo gitlab-runner start 
    sudo gitlab-runner register --non-interactive --name gitlabrunner --docker-image docker:stable --url https://gitlab.com --docker-volumes /var/run/docker.sock:/var/run/docker.sock --registration-token ${var.TOKEN} --executor docker   
    SHELL
}
output "ip" {
  value = "${google_compute_instance.gitrunner.network_interface.0.access_config.0.nat_ip}"
}
