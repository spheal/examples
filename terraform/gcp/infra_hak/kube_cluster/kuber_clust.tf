provider "google" {
  credentials = "${file("../keys/key.json")}"
  project = "${var.PROJECT}"
  region = "${var.REGION}"
}

resource "google_container_cluster" "kubernetes_cluster" {
    name = "kubernetes-cluster"
    location = "${var.REGION}-c"
    initial_node_count = 1

}

resource "google_container_node_pool" "kubernetes_node_cluster" {
    name = "kubernetes-cluster"
    location = "${var.REGION}-c"
    cluster = "${google_container_cluster.kubernetes_cluster.name}"
    node_config {
        preemptible  = true
        machine_type = "g1-small"
        disk_size_gb = 15
        disk_type = "pd-standard"
        image_type = "COS_CONTAINERD"
    }
    autoscaling {
        min_node_count = 1
        max_node_count = 3
    }
}


