variable "PROJECT" {}
variable "REGION" {
    default = "us-central1"
}
variable "TOKEN" {}
variable "ENVIR" {
    default = ["dev","prod"]
}
variable "SSHPATH" {
    default = "~/.ssh/id_rsa.pub"
}
variable "IMAGE_NAME" {
    default = "sample_app"
}

