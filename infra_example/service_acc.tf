resource "google_service_account" "container_pusher" {
  display_name = "container-pusher"
  account_id = "container-pusher"
}

resource "google_project_iam_binding" "container_pusher_role" {
  project = "${var.PROJECT}"
  role = "roles/storage.admin"
  members = ["serviceAccount:${google_service_account.container_pusher.email}"]
}

resource "google_service_account_key" "container_pusher_key" {
  service_account_id = "${google_service_account.container_pusher.name}"
}

data "google_service_account_key" "container_pusher_key" {
  name = "${google_service_account_key.container_pusher_key.name}"
  public_key_type = "TYPE_X509_PEM_FILE"
}
resource "local_file" "container_pusher_key_json" {
    content     = "${base64decode(google_service_account_key.container_pusher_key.private_key)}"
    filename = "keys/service_storage.json"
}
