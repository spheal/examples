#!/bin/bash
echo "Please enter you project-id:"
read PROJECT
gcloud auth activate-service-account --key-file keys/key.json
gsutil mb -l us-east1 -p $PROJECT -c regional gs://infra-deploy
gcloud services enable cloudresourcemanager.googleapis.com --project $PROJECT
gcloud services enable cloudidentity.googleapis.com --project $PROJECT
gcloud services enable container.googleapis.com --project $PROJECT
gcloud services enable iam.googleapis.com --project $PROJECT
gcloud services enable containerscanning.googleapis.com --project $PROJECT