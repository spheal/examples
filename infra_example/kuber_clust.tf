resource "google_container_cluster" "kubernetes_cluster" {
    name = "kubernetes-cluster"
    location = "${var.REGION}-c"
    remove_default_node_pool = true
    initial_node_count = 1

}
resource "google_container_node_pool" "kubernetes_node_cluster" {
    name = "kubernetes-cluster"
    location = "${var.REGION}-c"
    cluster = "${google_container_cluster.kubernetes_cluster.name}"
    node_count = 1
    node_config {
        preemptible  = true 
        machine_type = "n1-standard-4"
        disk_size_gb = 15
        disk_type = "pd-standard"
        image_type = "COS_CONTAINERD"
    }
    autoscaling {
        min_node_count = 1
        max_node_count = 3
    }
}
resource "null_resource" "get_kubectl_config" {
    provisioner "local-exec" {
        command = "gcloud container clusters get-credentials ${google_container_node_pool.kubernetes_node_cluster.name}"
    }
}



