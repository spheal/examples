# Don't forget to add . at the end of the dns name
resource "google_dns_managed_zone" "infra-deploy" {
    name = "infra-deploy"
    dns_name = "infra.deploy.${random_id.rnd.hex}.com."
    description = "DNS zone for sample_app and infrastructure"
}

resource "random_id" "rnd" {
    byte_length = 4
}
# Global static ip for ingress
resource "google_compute_global_address" "ingress-global" {
    name = "ingress-global-ip"
}
# A-record for gclobal ingress
resource "google_dns_record_set" "ingress-global-dnsname" {
    project = "${var.PROJECT}"
    managed_zone = "${google_dns_managed_zone.infra-deploy.name}"
    type = "A"
    ttl = 300
    rrdatas = ["${google_compute_global_address.ingress-global.address}"]
    name = "ingres.${google_dns_managed_zone.infra-deploy.dns_name}"
}



