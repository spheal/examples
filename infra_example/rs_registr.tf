# Retrieve data state from terraform backend
data "terraform_remote_state" "infra_state" {
    backend = "gcs" 
    config = { 
        prefix = "terraform/state"
        bucket = "infra-deploy"
        credentials = "keys/key.json"
    }
}


# Create container registry 
data "google_container_registry_repository" "docker_registry" {}
# Retrieve container registry  path
output "gcr_location" {
    value = "${data.google_container_registry_repository.docker_registry.repository_url}"
}
#
data "template_file" "gitlabci" {
  template = "${file("template/gitlabci.tlp")}"
  vars = {
    registry = "${data.google_container_registry_repository.docker_registry.repository_url}"
    image_name = "${var.IMAGE_NAME}"
    }
}
# Create local .gitlab-ci.yml file from template created on previous state
resource "local_file" "gitlab-ci" {
  content  = "${data.template_file.gitlabci.rendered}"
  filename = "../.gitlab-ci.yml"
}
data "template_file" "sonar_properties" {
  template = "${file("template/sonar-project.properties.tlp")}"
  vars = {
    app_name = "${var.IMAGE_NAME}"
    sonar_ip = "35.238.111.74"
  }
}
resource "local_file" "local_sonarp_roperties" {
  content = "${data.template_file.sonar_properties.rendered}"
  filename = "../sonar-project.properties"
}

