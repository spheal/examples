{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "ibext.connect.common.release.common_libs.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "ibext.connect.common.release.common_libs.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "ibext.connect.common.release.common_libs.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "ibext.connect.common.release.common_libs.labels" -}}
helm.sh/chart: {{ include "ibext.connect.common.release.chart" . }}
{{ include "ibext.connect.common.release.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "ibext.connect.common.release.common_libs.selectorLabels" -}}
app.kubernetes.io/name: {{ include "ibext.connect.common.release.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "ibext.connect.common.release.common_libs.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "ibext.connect.common.release.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}



{{/*
Deployment.yaml template for nested charts
*/}}


{{- define "ibext.connect.common.release.common_libs.deploymenttemplate" -}}
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ .Values.application.name }}
  namespace: {{ .Values.global.environment.namespace }}
spec:
  replicas: {{ .Values.application.container.replicas }}
  strategy: {}
  selector:
    matchLabels:
      app: {{ .Values.application.name }}
  template:
    metadata: 
      labels:
        app: {{ .Values.application.name }}
        aadpodidbinding: {{ .Values.global.environment.namespace }}-identity
      annotations:
        configmapHash: {{ .Values.global.application.configmaphash }}
    spec:
      containers:
      - image: {{ .Values.global.acr.name }}.azurecr.io/{{ .Values.global.acr.prefix }}/{{ .Values.application.image }}:{{ .Values.application.tag }}
        name: {{ .Values.application.name }}
        imagePullPolicy: Always
        command:
          [
            "java",
            "-javaagent:/applicationinsights-agent.jar"
          ]        
        envFrom:
        - configMapRef:
            name: {{ .Values.application.name }}-configmap
        ports:
        - containerPort: {{ .Values.global.container.port }}
        readinessProbe:
          httpGet:
            path: /{{ .Values.application.name }}/actuator/health
            port: 8080
          failureThreshold: 1
          periodSeconds: 30
            port: 8080
          failureThreshold: 1
          periodSeconds: 30
          initialDelaySeconds: 600
      restartPolicy: Always
{{- end }}



{{/*
ingress.yaml template for nested charts
*/}}

{{- define "ibext.connect.common.release.common_libs.ingresstemplate" -}}
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: {{ .Values.application.name }}-ingress
  namespace: {{ .Values.global.environment.namespace }}
  annotations:
    kubernetes.io/ingress.class: "nginx"
    nginx.ingress.kubernetes.io/ssl-redirect: "false"
spec:
  rules:
  - host: {{ .Values.global.fqdn }}
    http:
      paths:
      - path: /(.*)
        backend:
          serviceName: {{ .Values.application.name }}-service
          servicePort: {{ .Values.global.container.targetport }}
{{- end }}


{{/*
Service.yaml template for nested charts
*/}}

{{- define "ibext.connect.common.release.common_libs.servicetemplate" -}}
apiVersion: v1
kind: Service
metadata:
  labels:
  name: {{ .Values.application.name }}-service
  namespace: {{ .Values.global.environment.namespace }}
spec:
  type: LoadBalancer
  ports:
    - name: https
      port: 443
      targetPort: 8080
    - name: http
      port: 80
      targetPort: 8080
  selector:
    app: {{ .Values.application.name }}
status:
  loadBalancer: {}
{{- end }}